## Async Numbers

To complete the challenge, you need to generate a list of 50 unique numbers.
We provide the API you'll need to call to generate each individual number.

The implementation should be in the `solution.js` file.  This file should export a function that returns an array of numbers.  See the `test.js` file to see how it is used.

You must use the provided API function to get each individual number.  Here is an example of how the function is used:

``` javascript
var getNumber = require('./lib/get-number')
getNumber((number) => {
})
```

Note that `getNumber` is an asynchronous function that returns a random number after a random delay.

Do not modify the `test.js` file - this is used to validate your solution.  To validate your solution, run:

```
node test.js
```

## Notes
The solution consists of a helper function that makes API calls for us,
`generateNumbers`, and a run loop that continually checks how many unique
numbers we have received from our API calls. If we have enough unique numbers
we pass the first 50 unique integers to our callback function, `check` in the
test.

## Future Work
If this were a real API we would want to further refactor and optimize this
solution. Some things to consider:

- Better error handling. If this were a real API, we'd want to handle errors
from the network or server. Because the API was calling setTimeOut() and not
making any requests over the internet to a server, I left this out and only
opted to resolve with the random integer. Otherwise, we should be utilizing a
reject handler for our Promises and try and catch blocks that return
informative error messages.

- Making fewer calls to the API. Important to avoid rate limiting, optimizing
performance, etc. The number of requests I made was somewhat arbitrary, (50),
based on the fact that we used Math.random() and it tends to not be so random.

Otherwise, we'd want to spend more time optimizing how many API calls we make.
If we were to not use `Promise.all` we'd make fewer API calls but the solution
would be much slower. If the API was rate limited you might have to delay
requests.

## Lastly

Thanks for taking the time to consider me!

