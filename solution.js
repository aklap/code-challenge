var getNumber = require('./lib/get-number')

// Export a function that returns an array of 50 unique numbers.
// This function is called from `test.js`

module.exports = async function(callBack) {
  let unique = new Set();

  // Helper function makes API calls
  async function generateNums() {
    let nums = [];

    for(let i = 0; i < 5; i++) {
      nums.push(new Promise((resolve, reject) => getNumber(resolve)));
    }

    let result = await Promise.all(nums);
    return result;
  }

  // Run loop
  while(unique.size < 50) {
    const newNums = await generateNums();
    newNums.forEach(n => unique.add(n));
  }

  return callBack([...unique].slice(0, 50));
}

